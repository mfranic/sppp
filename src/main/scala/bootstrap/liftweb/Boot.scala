package bootstrap.liftweb

import net.liftweb.util.Props
import net.liftweb.common.Full
import net.liftweb.http.{LiftRules, S}
import net.liftweb.http.provider.HTTPRequest
import net.liftweb.mapper.{DB, Schemifier, DefaultConnectionIdentifier, StandardDBVendor}
import net.liftweb.db.ConnectionIdentifier
import sppp.model.User
import sppp.menu.MainSiteMap

class Boot
{
    def boot()
    {
        if (!DB.jndiJdbcConnAvailable_?)
        {
            val vendor =
                new StandardDBVendor(Props.get("db.driver") openOr "org.h2.Driver",
                    Props.get("db.url") openOr
                        "jdbc:h2:lift_proto.db;AUTO_SERVER=TRUE",
                    Props.get("db.user"), Props.get("db.password"))

            LiftRules.unloadHooks.append(vendor.closeAllConnections_! _)

            DB.defineConnectionManager(DefaultConnectionIdentifier.asInstanceOf[ConnectionIdentifier], vendor)
        }
        // where to search snippet
        LiftRules.addToPackages("sppp")

        LiftRules.addToPackages("com.damianhelme.tbutils")

        Schemifier.schemify(true, Schemifier.infoF _, User)

        LiftRules.setSiteMapFunc(() => MainSiteMap.siteMap)

        /*
        * Show the spinny image when an Ajax call starts
        */
        LiftRules.ajaxStart = Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

        /*
        * Make the spinny image go away when it ends
        */
        LiftRules.ajaxEnd = Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

        LiftRules.early.append(makeUtf8)

        LiftRules.loggedInTest = Full(() => User.loggedIn_?)

        S.addAround(DB.buildLoanWrapper())

    }

    private def makeUtf8(req: HTTPRequest)
    {
        req.setCharacterEncoding("UTF-8")
    }
}