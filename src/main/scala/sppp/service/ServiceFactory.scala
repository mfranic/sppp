package sppp.service

import net.liftweb.util.Props
import spj.shared.configuration.SpjRemotingClientConfiguration
import spj.shared.remoting.service.SpjRemotingService

object ServiceFactory
{
    private lazy val spjService = SpjRemotingClientConfiguration.build(Props.get("spj-remoting-url").open_!).getService

    def getSpjRemotingService: SpjRemotingService = spjService
}
