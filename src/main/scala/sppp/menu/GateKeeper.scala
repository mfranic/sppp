package sppp.menu

import net.liftweb.sitemap.Loc.If
import sppp.model.User
import net.liftweb.http.RedirectResponse

trait GateKeeper
{
    val loggedIn = If(() => User.loggedIn_?, () => RedirectResponse("/user_mgt/login"))
}
