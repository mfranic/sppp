package sppp.menu

import net.liftweb.sitemap.Menu
import net.liftweb.sitemap.Loc.Hidden

trait SnippetSiteMap extends GateKeeper
{
    lazy val siteMap: List[Menu] = List(Menu(manageEntry._1) / manageEntry._2 >> loggedIn,
        Menu(editEntry._1) / editEntry._2 >> loggedIn >> Hidden,
        Menu.i(createEntry._1) / createEntry._2 >> loggedIn >> Hidden)

    protected val editEntry : (String, String)

    protected val createEntry : (String, String)

    protected val manageEntry : (String, String)

}
