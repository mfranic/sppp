package sppp.menu

import sppp.snippet._
import sppp.model.User
import net.liftweb.sitemap.{Menu, SiteMap}
import net.liftweb.sitemap.Loc.PlaceHolder

object MainSiteMap extends GateKeeper
{

    lazy val siteMap = SiteMap(List(
        Menu("Home") / "index",
        Menu("Sifrarnici") / "sifrarnici/index" >> loggedIn >> PlaceHolder
            submenus (
            RazdjelSnippet.siteMap :::
                GlavaSnippet.siteMap :::
                KorisnikProracunaSnippet.siteMap :::
                OrgJedinicaSnippet.siteMap :::
                GlavniProgramSnippet.siteMap :::
                ProgramSnippet.siteMap :::
                IzvorFinSnippet.siteMap :::
                PartnerSnippet.siteMap :::
                MaticniPodaciSnippet.siteMap :::
                MjestoSnippet.siteMap :::
                FunkcijaSnippet.siteMap :::
                AktivnostSnippet.siteMap
            )) :::
        User.sitemap: _*
    )

}
