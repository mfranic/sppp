package sppp.snippet

object XhtmlContainer {

  val childBody =
    <child:entry>
      <tr>
        <td>
            <ch:id/>
        </td>
        <td>
            <ch:naziv/>
        </td>
      </tr>
    </child:entry>

  val resultTable =
      <children:title/> ++
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Naziv</th>
          </tr>
        </thead>
        <tbody>
            <children:body/>
        </tbody>
      </table>
}
