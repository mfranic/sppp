package sppp.model

import net.liftweb.mapper.{MetaMegaProtoUser, MegaProtoUser}
import net.liftweb.common.Full

object User extends User with MetaMegaProtoUser[User]
{
    override def dbTableName = "users"

    override def screenWrap = Full(<lift:surround with="default" at="content">
            <lift:bind/>
    </lift:surround>)

    override def fieldOrder = List(id, firstName, lastName, email, password)

    override def skipEmailValidation = true
}

class User extends MegaProtoUser[User]
{
    def getSingleton = User
}


